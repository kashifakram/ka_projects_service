﻿using Microsoft.AspNetCore.Mvc;
using Projects_Service.Model;
using Projects_Service.Model.Resources;
using Projects_Service.Services;
using System.Collections.Generic;

namespace Projects_Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService projectsService;
        public ProjectsController(IProjectsService projectsService)
        {
            this.projectsService = projectsService;
        }

        [HttpGet]
        public List<ProjectResource> GetAllProjects() => projectsService.GetProjects();

        [Route("addproject")]
        [HttpPost]
        public IActionResult AddSingleProject([FromBody] Projects proj)
        {
            var result = projectsService.AddProject(proj);

            if (result)
                return Created("",proj);

            return Forbid();
        }

        [Route("addprojectdesc")]
        [HttpPost]
        public IActionResult AddProjectDescription([FromBody] Project_Desc projDesc)
        {
            var result = projectsService.AddProjectDescription(projDesc);

            if (result)
                return Created("", projDesc);

            return StatusCode(403);
        }

        [Route("addprojectcat")]
        [HttpPost]
        public IActionResult AddProjectCategory([FromBody] Project_Category projCat)
        {
            var result = projectsService.AddProjectCategory(projCat);

            if (result)
                return Created("", projCat);

            return Forbid();
        }

        [Route("addcattoproj")]
        [HttpPost]
        public IActionResult AddCategoryToProject(int catId, int projId)
        {
            var cat = projectsService.AddProjectToExistingCategory(catId, projId);

            return Created("", cat);
        }

        [Route("adddesctoproj")]
        [HttpPost]
        public IActionResult AddDescriptionToProject(int descId, int projId)
        {
            var desc = projectsService.AddProjectToExistingDescription(descId, projId);

            if(desc != null)
                return Created("", desc);

            return StatusCode(404);
        }

        [Route("deleteproj")]
        [HttpDelete]
        public IActionResult DeleteProject(int projId)
        {
            var result = projectsService.DeleteProject(projId);

            if (result)
                return StatusCode(200);

            return StatusCode(404);
        }

        [Route("editproj")]
        [HttpPut]
        public IActionResult EditProject(Projects proj)
        {
            var result = projectsService.EditProject(proj);

            if (result)
                return StatusCode(200);

            return StatusCode(404);
        }

    }
}