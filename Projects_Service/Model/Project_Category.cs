﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Projects_Service.Model
{
    [Table("Project_Category")]
    public class Project_Category
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(500)]
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<Projects> Projects { get; set; }
    }
}
