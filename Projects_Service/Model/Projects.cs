﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projects_Service.Model
{
    public partial class Projects
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(1000)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Url { get; set; }
        public int? ProjectCategoryId { get; set; }
        [ForeignKey("ProjectCategoryId")]
        public Project_Category ProjectCategory { get; set; }
        public int? ProjectDescriptionId { get; set; }
        [ForeignKey("ProjectDescriptionId")]
        public Project_Desc ProjectDescription { get; set; }
    }
}