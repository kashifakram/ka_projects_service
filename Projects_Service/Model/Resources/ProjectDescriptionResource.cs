﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects_Service.Model.Resources
{
    public class ProjectDescriptionResource
    {
        public string ProjectTitle { get; set; }
        public string ProjectDescriptionDetails { get; set; }
    }
}
