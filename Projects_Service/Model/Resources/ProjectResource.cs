﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects_Service.Model.Resources
{
    public class ProjectResource
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectUrl { get; set; }
        public int ProjectCategoryId { get; set; }
        public int ProjectDescriptionId { get; set; }
    }
}