﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects_Service.Model.Resources
{
    public class ProjectCategory
    {
        public string CategoryName { get; set; }
        public string CategoryDescription { get; set; }
    }
}
