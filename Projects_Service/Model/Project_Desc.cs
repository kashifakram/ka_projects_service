﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projects_Service.Model
{
    [Table("Project_Desc")]
    public partial class Project_Desc
    {
        [Key]
        public int Id { get; set; }
        public string Description { get; set; }
        [MaxLength(800)]
        public string Title { get; set; }
        public ICollection<Projects> Projects { get; set; }
    }
}