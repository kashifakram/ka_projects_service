using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Projects_Service.Infrastructure;
using Projects_Service.Model;
using Projects_Service.Services;

namespace Projects_Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers()
                .AddNewtonsoftJson(options => options.UseCamelCasing(true));

            services.AddDbContext<ProjectContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("ProjectsConnectionString")));

            services.AddScoped<IProjectsService, ProjectsService>();

            services.AddAutoMapper(typeof(AutoMapperMappings));

            services.AddLogging();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                
            });
        }
    }
}
