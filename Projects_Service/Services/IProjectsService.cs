﻿using Projects_Service.Model;
using Projects_Service.Model.Resources;
using System.Collections.Generic;
using static Projects_Service.Services.ProjectsService;

namespace Projects_Service.Services
{
    public interface IProjectsService
    {
        public bool AddProject(Projects project);
        public bool AddProjectDescription(Project_Desc projectDesc);
        public object AddProjectToExistingCategory(int catId, int projectId);
        public object AddProjectToExistingDescription(int descId, int projectId);
        public bool AddProjectCategory(Project_Category projectCat);
        public List<ProjectResource> GetProjects();
        public bool DeleteProject(int id);
        public bool EditProject(Projects project);
    }
}
