﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Projects_Service.Model;
using Projects_Service.Model.Resources;
using System.Collections.Generic;
using System.Linq;

namespace Projects_Service.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly ProjectContext projectContext;
        private readonly ILogger<ProjectsService> logger;
        private readonly IMapper mapper;

        public ProjectsService(ProjectContext projectContext, ILogger<ProjectsService> logger, IMapper mapper)
        {
            this.projectContext = projectContext;
            this.logger = logger;
            this.mapper = mapper;
        }

        public bool AddProject(Projects project)
        {
            projectContext.Projects.Add(project);

            SaveToDatabase();

            return true;
        }

        public bool AddProjectDescription(Project_Desc projectDesc)
        {
            projectContext.Project_Desc.Add(projectDesc);

            SaveToDatabase();

            return true;
        }

        public bool AddProjectCategory(Project_Category projectCat)
        {
            projectContext.Project_Category.Add(projectCat);

            SaveToDatabase();

            return true;
        }

        public object AddProjectToExistingCategory(int catId, int projectId)
        {
            var cat = projectContext.Project_Category.FirstOrDefault(c => c.Id == catId);

            Projects proj;

            if (cat != null)
            {
                proj = projectContext.Projects.FirstOrDefault(p => p.Id == projectId);
                if (proj != null)
                    cat.Projects.Add(proj);
                else return null;
            }
            else return null;

            SaveToDatabase();

            return new { cat.Id, cat.Name, Project_Id = proj.Id, Project_Title = proj.Name };
        }

        public object AddProjectToExistingDescription(int descId, int projectId)
        {
            var desc = projectContext.Project_Desc.FirstOrDefault(c => c.Id == descId);

            Projects proj;

            if (desc != null)
            {
                proj = projectContext.Projects.FirstOrDefault(p => p.Id == projectId);
                if (proj != null)
                    desc.Projects.Add(proj);
                else
                    return null;
            }
            else
                return null;

            SaveToDatabase();

            return new { desc.Id, desc.Title, Project_Id = proj.Id, Project_Title = proj.Name };
        }

        public bool DeleteProject(int id)
        {
            var project = FindProject(id);
            if (project == null)
                return false;

            projectContext.Projects.Remove(project);

            SaveToDatabase();

            return true;
        }

        public bool EditProject(Projects project)
        {
            var resultRroject = FindProject(project.Id);

            if (resultRroject == null)
                return false;

            resultRroject.Name = project.Name;
            resultRroject.Url = project.Url;

            if (project.ProjectCategoryId != null)
                resultRroject.ProjectCategoryId = project.ProjectCategoryId;

            if (project.ProjectDescriptionId != null)
                resultRroject.ProjectDescriptionId = project.ProjectDescriptionId;

            SaveToDatabase();

            return true;
        }

        private void SaveToDatabase()
        {
            try
            {
                projectContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ducex)
            {
                logger.LogError("error occured during concurrent update", ducex.Message, ducex.StackTrace);
                throw;
            }
            catch (DbUpdateException duex)
            {
                logger.LogError("error occured during updating database", duex.Message, duex.StackTrace);
                throw;
            }
        }

        public List<ProjectResource> GetProjects() => 
           projectContext.Projects.Select(p => mapper.Map<ProjectResource>(p)).ToList();

        private Projects FindProject(int id) =>
            projectContext.Projects.FirstOrDefault(p => p.Id == id);

    }
}
