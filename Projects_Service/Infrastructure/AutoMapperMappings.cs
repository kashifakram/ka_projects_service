﻿using AutoMapper;
using Projects_Service.Model;
using Projects_Service.Model.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Projects_Service.Infrastructure
{
    public class AutoMapperMappings : Profile
    {
        public AutoMapperMappings()
        {
            CreateMap<Projects, ProjectResource>()
                .ForMember(dest => dest.ProjectId, options => options.MapFrom(src => src.Id))
                .ForMember(dest => dest.ProjectName, options => options.MapFrom(src => src.Name))
                .ForMember(dest => dest.ProjectUrl, options => options.MapFrom(src => src.Url))
                .ForMember(dest => dest.ProjectCategoryId, options => options.MapFrom(src => src.ProjectCategoryId))
                .ForMember(dest => dest.ProjectDescriptionId, options => options.MapFrom(src => src.ProjectDescriptionId));
        }
    }
}
